package com.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.security.auth.message.callback.PrivateKeyCallback.Request;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.EmployeeDao;
import com.dto.Employee;

@WebServlet("/GetEmployeeById")
public class GetEmployeeById extends HttpServlet {
	   
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		int empId = Integer.parseInt(request.getParameter("empId"));
		
		EmployeeDao empDao = new EmployeeDao();
		Employee emp = empDao.getEmployeeById(empId);
		
		
		if (emp != null) {
			
			request.setAttribute("emp", emp);
			
			RequestDispatcher rd = request.getRequestDispatcher("GetEmpById.jsp");
			rd.forward(request, response);
			
		} else {
			out.println("<br/><center>");
			
			RequestDispatcher rd = request.getRequestDispatcher("HRHomePage.jsp");
			rd.include(request, response);
			
			out.println("<br/><h2 style='color:red;'>Employee Record Not Found!!!</h2>");
			out.println("</center>");
		}
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}

